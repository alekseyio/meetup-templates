document.addEventListener('DOMContentLoaded', function() {
  /**
   * Sidebar toggler
   */
  const openSidebarBtn = document.querySelector('#open-sidebar-button');
  const closeSidebarBtn = document.querySelector('#close-sidebar-button');
  const sidebar = document.querySelector('#sidebar');
  const cls = 'sidebar--shown';

  openSidebarBtn.addEventListener('click', () => sidebar.classList.add(cls));
  closeSidebarBtn.addEventListener('click', () =>
    sidebar.classList.remove(cls)
  );

  /**
   * Sort & Filters togglers
   */
  const toggleSortBtn = document.querySelector('#toggle-sort-button');
  const toggleFiltersBtn = document.querySelector('#toggle-filters-button');
  const sortContainer = document.querySelector('#sort-container');
  const filtersContainer = document.querySelector('#filters-container');

  const toggle = (el, cls) => el.classList.toggle(cls);

  toggleSortBtn.addEventListener('click', () =>
    toggle(sortContainer, 'filters--shown')
  );
  toggleFiltersBtn.addEventListener('click', () =>
    toggle(filtersContainer, 'filters--shown')
  );
});
